FROM debian:bullseye-backports as extractor
RUN apt-get update
RUN apt-get install wget pigz -y
RUN wget -q -O qt.tar.gz https://vault.pkasila.net/qt/6.7.1/qt_linux_x86_64.tar.gz
RUN mkdir -p /opt/qt
RUN pigz -dc qt.tar.gz | tar xf - -C /opt/qt

FROM debian:bullseye-backports

COPY --from=extractor /opt/qt/6.7.1/gcc_64 /opt/qt

RUN apt-get update
RUN apt-get install apt-transport-https curl wget git gnupg build-essential cmake tree lsb-release software-properties-common -y
RUN curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor >bazel-archive-keyring.gpg
RUN mv bazel-archive-keyring.gpg /usr/share/keyrings
RUN echo "deb [arch=amd64 signed-by=/usr/share/keyrings/bazel-archive-keyring.gpg] https://storage.googleapis.com/bazel-apt stable jdk1.8" | tee /etc/apt/sources.list.d/bazel.list
RUN apt-get update
RUN apt-get install -y bazel-6.5.0 python3

RUN ln -s /usr/bin/bazel-6.5.0 /usr/bin/bazel

RUN wget https://apt.llvm.org/llvm.sh
RUN chmod +x llvm.sh
RUN ./llvm.sh 18 all

RUN ln -s /usr/bin/clang-tidy-18 /usr/bin/clang-tidy
RUN ln -s /usr/bin/clang-format-18 /usr/bin/clang-format

RUN groupadd --gid 1000 qtbuilder \
    && useradd --uid 1000 --gid 1000 -m qtbuilder

USER qtbuilder
